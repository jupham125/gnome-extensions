// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

const Lang = imports.lang;
const St = imports.gi.St;
const Main = imports.ui.main;
const Gtk = imports.gi.Gtk;
const Gio = imports.gi.Gio;
const Signals = imports.signals;
const Tweener = imports.ui.tweener;
const Util = imports.misc.util;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;
const GLib = imports.gi.GLib;
const Gettext = imports.gettext.domain('gnome-shell-extensions');
const _ = Gettext.gettext;

const Me = imports.misc.extensionUtils.getCurrentExtension();
const DBusIface = Me.imports.dbus;
const dialog = Me.imports.dialog;

let wifiSubMenu = null;
var netctl_dbus = null;

var wireless_symbols = {
    'offline':'network-wireless-offline-symbolic',
    'connected':'network-wireless-connected-symbolic',
    'acquiring':'network-wireless-acquiring-symbolic',
    'encrypted':'network-wireless-encrypted-symbolic',
    'none':'network-wireless-signal-none-symbolic',
    'Very Weak':'network-wireless-signal-weak-symbolic',
    'Weak':'network-wireless-signal-ok-symbolic',
    'Fair':'network-wireless-signal-good-symbolic',
    'Very Good':'network-wireless-signal-excellent-symbolic',
    'Excellent':'network-wireless-signal-excellent-symbolic',
    'Unknown':'network-error-symbolic',
    'error':'network-error-symbolic'
};

var AggregateMenuIndicator = new Lang.Class({
    Name: 'AggregateMenuIndicator',
    Extends: PanelMenu.SystemIndicator,

    _init: function() {
        this.parent();
        this._primaryIndicator = this._addIndicator();
        this._primaryIndicator.icon_name = "network-wireless-offline-symbolic";
        this._primaryIndicator.style_class = "system-status-icon no-padding";
        this.indicators.show();      
    },
});

const WifiSubMenu = new Lang.Class({
    Name: 'WifiSubMenu',
    Extends: PopupMenu.PopupSubMenuMenuItem,

    _init: function() {
        this.parent('', true);
        this.active_ssid = null;

        this.selectItem = new PopupMenu.PopupMenuItem('Select Network');
        this.selectItem.connect('activate', Lang.bind(this, this._selectNetwork));
        this.menu.addMenuItem(this.selectItem);

        this.hiddenItem = new PopupMenu.PopupMenuItem('Connect to Hidden');
        this.hiddenItem.connect('activate', Lang.bind(this, this._showHiddenDialog));
        this.menu.addMenuItem(this.hiddenItem);

        this.disconnectItem = new PopupMenu.PopupMenuItem("Disconnect");
        this.disconnectItem.connect('activate', Lang.bind(this, this._disconnectNetwork));
        this.menu.addMenuItem(this.disconnectItem);

        /*
        // These menu entries may be used at a later date.
        this.toggleItem = new PopupMenu.PopupSwitchMenuItem("Turn Off");
        this.toggleItem.connect('activate', Lang.bind(this, this._toggleNetwork));
        this.menu.addMenuItem(this.toggleItem);

        this.settingsItem = new PopupMenu.PopupMenuItem("Network Settings");
        this.settingsItem.connect('activate', Lang.bind(this, this._networkSettings));
        this.menu.addMenuItem(this.settingsItem);
        */

        this.netlist = [ ];

        this.indicator = new AggregateMenuIndicator();

        this.unknown();
    },

    _showHiddenDialog: function() {
        this._hiddenDialog = new dialog.SecurityTypeDialog();
        this._hiddenDialog.connect('closed', this._hiddenDialogClosed.bind(this));

        this._hiddenDialog.open();
    },

    _showDialog: function() {
        this._dialog = new dialog.WifiSelectDialog(0, 0, this.active_ssid);
        this._dialog.connect('closed', this._dialogClosed.bind(this));


        for (let network of this.netlist) {
            if (!network.hasOwnProperty('ssid')) {
                continue;
            }
            let ap = new dialog.AccessPoint(network.ssid, network.key_mgmt, network.signal_strength)
            let apsec = dialog.apsecurity[network.key_mgmt];
            let net = {
                ssid: network.ssid,
                key_mgmt: network.key_mgmt,
                security: apsec,
                connections: [ ],
                item: null,
                accessPoints: [ ap ],
                signal_strength: network.signal_strength
            };
            this._dialog._accessPointAdded(null, net);
            
        }
        // ssid, signal_strength, key_mgmt: wpa-psk
        this._dialog.open();
    },

    _hiddenDialogClosed() {
        this._hiddenDialog = null;
    },

    _dialogClosed() {
        //this._dialog.destroy();
        this._dialog = null;
    },

    _populateNetworkSelect: function(ssidlist) {

    },

    connecting: function() {
        this.wirelessConnected = false;

        this.updateNetworkName("Connecting ...");
        this.updateIcon("acquiring");
    },

    connectedTo: function(ssid) {
        this.wirelessConnected = true;

        this.active_ssid = ssid;
        this.updateNetworkName(ssid);
    },

    disconnected: function () {
        this.wirelessConnected = false;

        this.active_ssid = null;
        this.updateNetworkName("Wi-Fi not connected");
        this.updateIcon("offline");
    },

    portal: function() {
        this.updateNetworkName("Portal");
        this.updateIcon("acquiring");
    },

    unknown: function() {
        this.wirelessConnected = false;

        this.updateNetworkName("Unknown state");
        this.updateIcon("acquiring");
    },

    failure: function() {
        this.active_ssid = null;
        this.updateNetworkName("netctl service unavailable");
        this.updateIcon("offline");
    },
    
    updateIcon: function(signal) {
        if (wireless_symbols.hasOwnProperty(signal)) {
            this.icon.icon_name = wireless_symbols[signal];
            if (this.indicator) {
                this.indicator._primaryIndicator.icon_name = wireless_symbols[signal];
            }
        }
    },

    updateIP: function(ip) {
        this._ipAddress = ip;
    },
    updateNetworkName: function(name) {
        this.label.set_text(name);
    },
    
    _selectNetwork: function() {
        this._showDialog();
    },
    
    _toggleNetwork: function() {

    },
    connect_to_network: function(net, secret) {
        let self = this;

        let obj = {
            ssid: net.ssid,
            scan_ssid: net.scan_ssid,
            psk: secret.psk,
            key_mgmt: net.security,
            identity: secret.identity,
            password: secret.password,
            eap: secret.eap,
            ca_cert: secret.cacert,
            client_cert: secret.clientcert,
            priv_key: secret.privkey,
            priv_key_password: secret.privkeypass
        };

        netctl_dbus.WirelessConnect(
            obj,
            (s) => {
                let state = s[0];

                let stateUndefined = (state === undefined || state === null || state === "");

                // Log connection error
                if (stateUndefined) {
                    global.log("connection error: invalid configuration");
                }

                // check for failed states in reply
                if (net.key_mgmt != "none"
                    && (state == netctl_dbus.UPDATE_STATES.FAILED
                        || stateUndefined
                        || state == netctl_dbus.UPDATE_STATES.UNKNOWN))
                {
                    self.passwordDialog = new dialog.WifiPasswordDialog(net, true);
                    self.passwordDialog.open(global.get_current_time());
                // check for unknown state if network is hidden
                } else if (net.scan_ssid && net.scan_ssid == 1
                    && (state == netctl_dbus.UPDATE_STATES.UNKNOWN || stateUndefined))
                {
                    self.hiddenDialog = new dialog.HiddenNetworkDialog({ key: "none" }, net.ssid);
                    self.hiddenDialog.open(global.get_current_time());
                }
            }
        );
    },
    
    _disconnectNetwork: function() {
        netctl_dbus.WirelessDisconnect();
    },
    
    _networkSettings: function() {

    },

    destroy: function() {
        this.parent();
    }
    
});

function init() {
}

function enable() {
    wifiSubMenu = new WifiSubMenu();

    // Try to add the output-switcher right below the output slider...
    let aggregateMenuPanelButton = Main.panel.statusArea['aggregateMenu'];
    let powerIndicator = aggregateMenuPanelButton._power;
    let desiredMenuPosition = aggregateMenuPanelButton.menu._getMenuItems().indexOf(Main.panel.statusArea.aggregateMenu._rfkill.menu);
    let powerSubmenuPosition = aggregateMenuPanelButton.menu._getMenuItems().indexOf(powerIndicator.menu);
    aggregateMenuPanelButton._indicators.insert_child_below(wifiSubMenu.indicator.indicators, powerIndicator.indicators);

    let menu = Main.panel.statusArea.aggregateMenu.menu;
    menu.addMenuItem(wifiSubMenu, desiredMenuPosition);
    
    netctl_dbus = new DBusIface.netctlDBusInterface(wifiSubMenu);

    global.log('Wi-Fi shell extension started');
}

function disable() {
    global.log('Wi-Fi shell extension terminating');
    netctl_dbus.disable();
    wifiSubMenu.indicator.indicators.destroy();
    wifiSubMenu.indicator = null;
    wifiSubMenu.destroy();
    wifiSubMenu = null;
}

