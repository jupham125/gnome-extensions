// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

const Gio = imports.gi.Gio;
const Lang = imports.lang;
const GLib = imports.gi.GLib;

const netctlIface = '\
<node>\
    <interface name="com.gitlab.redfield.netctl.wireless">\
        <method name="WirelessConnect">\
                <arg direction="in" type="s"/>\
                <arg direction="out" type="i"/>\
        </method>\
        <method name="WirelessDisconnect">\
        </method>\
        <signal name="PropertiesChanged">\
                <arg type="s" direction="out"/>\
        </signal>\
    </interface>\
</node>';

const gnomePortalHelperIface = '\
<node>\
    <interface name="org.gnome.Shell.PortalHelper">\
        <method name="Authenticate">\
          <arg name="connection" type="o" direction="in"/>\
          <arg name="url" type="s" direction="in"/>\
          <arg name="timestamp" type="u" direction="in"/>\
        </method>\
        <method name="Close">\
          <arg name="connection" type="o" direction="in"/>\
        </method>\
        <method name="Refresh">\
          <arg name="connection" type="o" direction="in"/>\
        </method>\
        <signal name="Done">\
          <arg type="o" name="connection"/>\
          <arg type="u" name="result"/>\
        </signal>\
    </interface>\
</node>';

const NetctlIfaceProxy = Gio.DBusProxy.makeProxyWrapper(netctlIface);
const PortalHelperProxy = Gio.DBusProxy.makeProxyWrapper(gnomePortalHelperIface);

var netctlDBusInterface = new Lang.Class({
    Name: "netctlDBusInterface",

    _init: function(menuObject) {
        let self = this;

        this.menu = menuObject;

        this.UPDATE_TYPES = {
            UNSPECIFIED: 0,
            STATE: 1,
            IP: 2,
            SIGNAL: 3,
            AVAILABLE_NETWORKS: 4,
            STATS: 5,
        };

        this.UPDATE_STATES = {
            UNKNOWN: 0,
            DISCONNECTED: 1,
            CONNECTED: 2,
            CONNECTING: 3,
            PORTAL: 4,
            FAILED: 5,
        };

        new NetctlIfaceProxy(
            Gio.DBus.system,
            'com.gitlab.redfield.netctl.wireless',
            '/com/gitlab/redfield/netctl/wireless',
             (obj, error) => {
                 if (error) {
                     global.log("Wi-Fi shell extension", "error launching server proxy");
                 }

                 self.server_proxy = obj;

                // Assign remote methods to DBus interface object as first-class properties
                 Object.keys(this.server_proxy).forEach(
                    // Search for remote methods within "server_proxy" object
                    (key) => {
                        if (key.includes("Remote")
                            && typeof self.server_proxy[key] == "function"
                            && self.server_proxy.hasOwnProperty(key)
                        ) {
                              self[key.replace("Remote", "")] = (obj, cb) => {
                                   let args = [];

                                   if (obj != undefined) {
                                       stripEmptyProperties(obj);

                                       args.push(JSON.stringify(obj));
                                   }

                                   if (cb) {
                                       args.push(cb);
                                   }

                                  self.server_proxy[key](...args);
                              };
                          }
                    }
                 );

                 self._updateSignalId = self.server_proxy.connectSignal(
                     "PropertiesChanged",
                     (proxy, sender, s) => {
                         self.updateStatus(s);
                     });
             }
        );

        this.lastUpdate = 0;
        this.monitorUpdates(1);
    },

    // Ensure netctl is consistently sending status updates, indicate failure if not
    monitorUpdates: function(lastUpdate){
        let self = this;

        GLib.timeout_add_seconds(
            GLib.PRIORITY_DEFAULT,
            16,
            () => {
                if (lastUpdate == self.lastUpdate) {
                    self.menu.failure();
                }

                self.monitorUpdates(self.lastUpdate);

                return GLib.SOURCE_REMOVE;
            }
        );
    },

    updateStatus: function (s) {
        let self = this;
        let update = JSON.parse(s);

        this.lastUpdate = Math.random();

        switch(update.type) {
        case this.UPDATE_TYPES.AVAILABLE_NETWORKS:
            this.menu.netlist = update.props.access_points
                .filter((ap) => {
                    return ap.ssid != "";
                });

            if (update.props.hasOwnProperty("signal_strength")) {
                this.menu.updateIcon(update.props.signal_strength);
            }

            this.menu.iface = update.props.iface_name;

            if (!update.props.hasOwnProperty("state")) {
                break;
            }
        case this.UPDATE_TYPES.STATE:
            let state = update.props.state;

            switch (state) {
            case this.UPDATE_STATES.CONNECTED: //"connected":
                this.menu.connectedTo(update.props.ssid);
                this.menu.iface = update.props.iface_name;
                break;
            case this.UPDATE_STATES.DISCONNECTED: //"disconnected":
                this.menu.disconnected();
                break;
            case this.UPDATE_STATES.CONNECTING: // connecting
                this.menu.connecting();
                break;
            case this.UPDATE_STATES.PORTAL: // portal
                this.menu.portal();
                // Launch portal login page
                this.launchPortalHelper(update.props.portal_url)
                break;
            default:
                this.menu.unknown();
            }
            break;

        case this.UPDATE_TYPES.IP:
            this.menu.updateIP(update.props.ip_address);
            break;

        case this.UPDATE_TYPES.SIGNAL:
            if (update.props.hasOwnProperty("signal_strength") && this.menu.wirelessConnected) {
                this.menu.updateIcon(update.props.signal_strength);
            }
            break;
        default:
        }
    },

    launchPortalHelper: function(url) {
        // Call org.gnome.Shell.PortalHelper
        // Requires arguements: DBus object path, URL, timestamp
        let portalHelper = new PortalHelperProxy(
            Gio.DBus.session,
            'org.gnome.Shell.PortalHelper',
            '/org/gnome/Shell/PortalHelper'
        );

        // Make timestamp
        let ts = Math.round((new Date()).getTime() / 1000);

        // Make the call to org.gnome.Shell.PortalHelper.Authenticate, asynchronously
        portalHelper.AuthenticateRemote(portalHelper.g_object_path, url, ts)
    },

    disable: function() {
        if (this._updateSignalId)
            this.server_proxy.disconnectSignal(this._updateSignalId);
    }
})

// Remove properties without a concrete value from message objects
const stripEmptyProperties = (obj) => {
    if (typeof obj == "object") {
        Object.keys(obj).forEach((objKey) => {
            if (obj.hasOwnProperty(objKey)
                && (obj[objKey] === undefined
                || obj[objKey] === null
                || obj[objKey] === "")
            ) {
                   delete(obj[objKey]);
            } else if (typeof obj[objKey] === "object") {
                stripEmptyProperties(obj[objKey]);
            }
        });
    }
};

