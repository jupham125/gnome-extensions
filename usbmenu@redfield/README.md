# wifimenu@redfield

gnome-shell-extension for wifi control

install.sh copies this directory into ~/.local/share/gnome-extensions/

## gnome-shell output

sudo journalctl /usr/bin/gnome-shell -f -o cat

## issues

### If you need to disable extension from command line
```
gnome-shell-extension-tool -d wifimenu@redfield
```

### If gnome shell crashes while you're logged in switch to a different virtual console and run
```
sudo /etc/init.d/gdm3 restart
```

Sometimes the wifi won't connect on first attempt. Selecting the network again should connect.


## To get an IP address automatically on connect
/etc/systemd/network/10-wlan0.network
```
[Match]
Name=wlan0

[Network]
DHCP=ipv4
```
sudo systemctl start systemd-networkd
