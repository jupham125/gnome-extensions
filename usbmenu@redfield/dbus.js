// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

const Gio = imports.gi.Gio;
const Lang = imports.lang;
const GLib = imports.gi.GLib;

const grpcServerIface = '\
<node>\
        <interface name="com.gitlab.redfield.api.usbctl">\
                <method name="Attach">\
                        <arg name="cmd" direction="in" type="s"/>\
                        <arg direction="out" type="s"/>\
                </method>\
                <method name="Detach">\
                        <arg name="cmd" direction="in" type="s"/>\
                        <arg direction="out" type="s"/>\
                </method>\
                <method name="DeviceList">\
                        <arg direction="out" type="s"/>\
                </method>\
                <method name="VmList">\
                        <arg direction="out" type="s"/>\
                </method>\
        </interface>\
</node>';

var pbtypes = {
    // Update type identifers
    updateUsbDevices: 0,
    updateVms: 1,

    // USB Control Commands
    cmdAttachDevice: 0,
    cmdDetachDevice: 1,
};

const ServerIfaceProxy = Gio.DBusProxy.makeProxyWrapper(grpcServerIface);

var usbDbusServer = new Lang.Class({
    Name: "usbDbusServer",

    _init: function(menuObject) {
        this.menu = menuObject;
        new ServerIfaceProxy(Gio.DBus.system, 'com.gitlab.redfield.api.usbctl', '/com/gitlab/redfield/api/usbctl',
                             (obj, error) => {
                                 if (error) {
                                     global.log("USB Assignment shell extension", "error launching server proxy");
                                 }
                                 this.server_proxy = obj;
                             });
    },

    AttachDevice: function (obj) {
        var s = JSON.stringify(obj);
        var status = this.server_proxy.AttachSync(s);

        return status
    },
    DetachDevice: function(obj) {
        var s = JSON.stringify(obj);
        var status = this.server_proxy.DetachSync(s);

        return status
    },
    VmList: function() {
        var obj = this.server_proxy.VmListSync();

        return JSON.parse(obj)
    },
    DeviceList: function() {
        var obj = this.server_proxy.DeviceListSync();

        return JSON.parse(obj)
    },
    disable: function() {
        if (this._updateSignalId)
            this.server_proxy.disconnectSignal(this._updateSignalId);
    }
})

