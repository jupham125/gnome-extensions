// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// This file is largely composed of code snippets pieced together from
// gnome-shell js/ui/components/networkAgent.js and js/ui/status/network.js
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const ModalDialog = imports.ui.modalDialog;
const Dialog = imports.ui.dialog;
const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const Animation = imports.ui.animation;
const Signals = imports.signals;
const Util = imports.misc.util;
const Main = imports.ui.main;
const Pango = imports.gi.Pango;
const ShellEntry = imports.ui.shellEntry;
const Shell = imports.gi.Shell;
const PopupMenu = imports.ui.popupMenu;
const Atk = imports.gi.Atk;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const DBusIface = Me.imports.dbus;
const usbmenu = Me.imports.extension;

var UsbDeviceDialogItem = new Lang.Class({
    Name: 'UsbDeviceDialogItem',

    _init(usbDevice) {
        this._usbDevice = usbDevice;

        this.actor = new St.BoxLayout({ style_class: 'nm-dialog-item',
                                        can_focus: true,
                                        reactive: true });
        this.actor.connect('key-focus-in', () => { this.emit('selected'); });
        let action = new Clutter.ClickAction();
        action.connect('clicked', () => { this.actor.grab_key_focus(); });
        this.actor.add_action(action);

        let title = this._usbDevice.name;
        this._label = new St.Label({ text: title });

        this.actor.label_actor = this._label;
        this.actor.add(this._label, { x_align: St.Align.START });

        this._icons = new St.BoxLayout({ style_class: 'nm-dialog-icons' });
        this.actor.add(this._icons, { expand: true, x_fill: false, x_align: St.Align.END });
    },

    setActive(isActive) {
        this._selectedIcon.opacity = isActive ? 255 : 0;
    },
});
Signals.addSignalMethods(UsbDeviceDialogItem.prototype);

var VmDialogItem = new Lang.Class({
    Name: 'VmDialogItem',

    _init(vm) {
        this._vm = vm;

        this.actor = new St.BoxLayout({ style_class: 'nm-dialog-item',
                                        can_focus: true,
                                        reactive: true });
        this.actor.connect('key-focus-in', () => { this.emit('selected'); });
        let action = new Clutter.ClickAction();
        action.connect('clicked', () => { this.actor.grab_key_focus(); });
        this.actor.add_action(action);

        let title = this._vm.config.c_info.name;
        this._label = new St.Label({ text: title });

        this.actor.label_actor = this._label;
        this.actor.add(this._label, { x_align: St.Align.START });

        this._icons = new St.BoxLayout({ style_class: 'nm-dialog-icons' });
        this.actor.add(this._icons, { expand: true, x_fill: false, x_align: St.Align.END });
    },

    setActive(isActive) {
        this._selectedIcon.opacity = isActive ? 255 : 0;
    },
});
Signals.addSignalMethods(VmDialogItem.prototype);

var VmAssignmentDialog = new Lang.Class({
    Name: 'VmAssignmentDialog',
    Extends: ModalDialog.ModalDialog,

    _init: function (usbDevice) {
        this.parent({ styleClass: 'nm-dialog' });
        this._buildLayout(usbDevice);
        this.usbdevice = usbDevice
        let id = Main.sessionMode.connect('updated', () => {
            if (Main.sessionMode.allowSettings)
                return;

            Main.sessionMode.disconnect(id);
            this.close();
        });
    },

    _buildLayout(usbDevice) {
        let headline = new St.BoxLayout({ style_class: 'nm-dialog-header-hbox' });

        let titleBox = new St.BoxLayout({ vertical: true });
        let title = new St.Label({ style_class: 'nm-dialog-header',
                                   text: _("Virtual Machines") });
        let subtitle = new St.Label({ style_class: 'nm-dialog-subheader',
                                      text: _("Select a guest to attach " + usbDevice.name + " to") });
        titleBox.add(title);
        titleBox.add(subtitle);
        headline.add(titleBox);

        this.contentLayout.style_class = 'nm-dialog-content';
        this.contentLayout.add(headline);

        this._stack = new St.Widget({ layout_manager: new Clutter.BinLayout() });

        this._itemBox = new St.BoxLayout({ vertical: true });
        this._scrollView = new St.ScrollView({ style_class: 'nm-dialog-scroll-view' });
        this._scrollView.set_x_expand(true);
        this._scrollView.set_y_expand(true);
        this._scrollView.set_policy(Gtk.PolicyType.NEVER,
                                    Gtk.PolicyType.AUTOMATIC);
        this._scrollView.add_actor(this._itemBox);
        this._stack.add_child(this._scrollView);

        this.contentLayout.add(this._stack, { expand: true });

        this._disconnectButton = this.addButton({ action: this.close.bind(this),
                                                  label: _("Cancel"),
                                                  key: Clutter.Escape });
        this._connectButton = this.addButton({ action: this._attach.bind(this),
                                               label: _("Attach"),
                                               key: Clutter.Return });
    },

    _selectVm(vm) {
        if (this._selectedVm)
            this._selectedVm.item.actor.remove_style_pseudo_class('selected');

        this._selectedVm = vm;

        if (this._selectedVm)
            this._selectedVm.item.actor.add_style_pseudo_class('selected');
    },

    _createVmItem(vm) {
        vm.item = new VmDialogItem(vm)
        vm.item.connect('selected', () => {
            Util.ensureActorVisibleInScrollView(this._scrollView, vm.item.actor);
            this._selectVm(vm);
        });
        vm.item.actor.connect('destroy', () => {
            let keyFocus = global.stage.key_focus;
            if (keyFocus && keyFocus.contains(vm.item.actor))
                this._itemBox.grab_key_focus();
        });
    },

    _vmAdded(vm) {
        let pos = -1;

        this._createVmItem(vm);
        this._itemBox.insert_child_at_index(vm.item.actor, pos);
    },

    destroy: function() {
        this.parent();
    },

    _attach: function() {
        var clean_device = {
            name: this.usbdevice.name,
            bus: this.usbdevice.bus,
            port: this.usbdevice.port,
            addr: this.usbdevice.addr,
            speed: this.usbdevice.speed,
            spec: this.usbdevice.spec,
            device_version: this.usbdevice.device_version,
            vendor_id: this.usbdevice.vendor_id,
            product_id: this.usbdevice.product_id,
            class: this.usbdevice.class,
            subclass: this.usbdevice.subclass,
            protocol: this.usbdevice.subclass
        };

        var request = {
            domid: this._selectedVm.domid.toString(),
            device: clean_device
        };

        var status = usbmenu.dbus_server.AttachDevice(request);

        this.close();
    },
});

var UsbAssignDialog = new Lang.Class({
    Name: 'UsbAssignDialog',
    Extends: ModalDialog.ModalDialog,

    _init: function () {
        this.parent({ styleClass: 'nm-dialog' });

        this._buildLayout();

        let id = Main.sessionMode.connect('updated', () => {
            if (Main.sessionMode.allowSettings)
                return;

            Main.sessionMode.disconnect(id);
            this.close();
        });

    },

    _buildLayout() {
        let headline = new St.BoxLayout({ style_class: 'nm-dialog-header-hbox' });

        let titleBox = new St.BoxLayout({ vertical: true });
        let title = new St.Label({ style_class: 'nm-dialog-header',
                                   text: _("USB Devices") });
        let subtitle = new St.Label({ style_class: 'nm-dialog-subheader',
                                      text: _("Select a USB device for assignment") });
        titleBox.add(title);
        titleBox.add(subtitle);
        headline.add(titleBox);

        this.contentLayout.style_class = 'nm-dialog-content';
        this.contentLayout.add(headline);

        this._stack = new St.Widget({ layout_manager: new Clutter.BinLayout() });

        this._itemBox = new St.BoxLayout({ vertical: true });
        this._scrollView = new St.ScrollView({ style_class: 'nm-dialog-scroll-view' });
        this._scrollView.set_x_expand(true);
        this._scrollView.set_y_expand(true);
        this._scrollView.set_policy(Gtk.PolicyType.NEVER,
                                    Gtk.PolicyType.AUTOMATIC);
        this._scrollView.add_actor(this._itemBox);
        this._stack.add_child(this._scrollView);

        this.contentLayout.add(this._stack, { expand: true });

        this._cancelButton = this.addButton({ action: this.close.bind(this),
                                                  label: _("Cancel"),
                                                  key: Clutter.Escape });
        this._assignButton = this.addButton({ action: this._assign.bind(this),
                                              label: _("Assign"),
                                              key: Clutter.Return });
    },

    _selectUsbDevice(usbDevice) {
        if (this._selectedUsbDevice)
            this._selectedUsbDevice.item.actor.remove_style_pseudo_class('selected');

        this._selectedUsbDevice = usbDevice;

        if (this._selectedUsbDevice)
            this._selectedUsbDevice.item.actor.add_style_pseudo_class('selected');
    },

    _createUsbDeviceItem(usbDevice) {
        usbDevice.item = new UsbDeviceDialogItem(usbDevice)
        usbDevice.item.connect('selected', () => {
            Util.ensureActorVisibleInScrollView(this._scrollView, usbDevice.item.actor);
            this._selectUsbDevice(usbDevice);
        });
        usbDevice.item.actor.connect('destroy', () => {
            let keyFocus = global.stage.key_focus;
            if (keyFocus && keyFocus.contains(usbDevice.item.actor))
                this._itemBox.grab_key_focus();
        });
    },

    _usbDeviceAdded(usbDevice) {
        let pos = -1;

        this._createUsbDeviceItem(usbDevice);
        this._itemBox.insert_child_at_index(usbDevice.item.actor, pos);
    },

    destroy: function() {
        this.parent();
    },

    _assign: function() {
        this.assignmentDialog = new VmAssignmentDialog(this._selectedUsbDevice);

        this.vmlist = usbmenu.dbus_server.VmList();

        for (let vm in this.vmlist) {
            // We can't assign to PV(H) guests right now.
            if (this.vmlist[vm].config.c_info.type == 'pv' || this.vmlist[vm].config.c_info.type == 'pvh') {
                continue;
            }

            this.assignmentDialog._vmAdded(this.vmlist[vm]);
        }

        this.assignmentDialog.open(global.get_current_time());
        this.close();
    },
});

var UsbRemoveDialog = new Lang.Class({
    Name: 'UsbRemoveDialog',
    Extends: ModalDialog.ModalDialog,

    _init: function () {
        this.parent({ styleClass: 'nm-dialog' });

        this._buildLayout();

        let id = Main.sessionMode.connect('updated', () => {
            if (Main.sessionMode.allowSettings)
                return;

            Main.sessionMode.disconnect(id);
            this.close();
        });

    },

    _buildLayout() {
        let headline = new St.BoxLayout({ style_class: 'nm-dialog-header-hbox' });

        let titleBox = new St.BoxLayout({ vertical: true });
        let title = new St.Label({ style_class: 'nm-dialog-header',
                                   text: _("Assigned USB Devices") });
        let subtitle = new St.Label({ style_class: 'nm-dialog-subheader',
                                      text: _("Select a USB device for removal") });
        titleBox.add(title);
        titleBox.add(subtitle);
        headline.add(titleBox);

        this.contentLayout.style_class = 'nm-dialog-content';
        this.contentLayout.add(headline);

        this._stack = new St.Widget({ layout_manager: new Clutter.BinLayout() });

        this._itemBox = new St.BoxLayout({ vertical: true });
        this._scrollView = new St.ScrollView({ style_class: 'nm-dialog-scroll-view' });
        this._scrollView.set_x_expand(true);
        this._scrollView.set_y_expand(true);
        this._scrollView.set_policy(Gtk.PolicyType.NEVER,
                                    Gtk.PolicyType.AUTOMATIC);
        this._scrollView.add_actor(this._itemBox);
        this._stack.add_child(this._scrollView);

        this.contentLayout.add(this._stack, { expand: true });

        this._cancelButton = this.addButton({ action: this.close.bind(this),
                                                  label: _("Cancel"),
                                                  key: Clutter.Escape });
        this._removeButton = this.addButton({ action: this._remove.bind(this),
                                              label: _("Remove"),
                                              key: Clutter.Return });
    },

    _selectUsbDevice(usbDevice) {
        if (this._selectedUsbDevice)
            this._selectedUsbDevice.item.actor.remove_style_pseudo_class('selected');

        this._selectedUsbDevice = usbDevice;

        if (this._selectedUsbDevice)
            this._selectedUsbDevice.item.actor.add_style_pseudo_class('selected');
    },

    _createUsbDeviceItem(usbDevice) {
        usbDevice.item = new UsbDeviceDialogItem(usbDevice)
        usbDevice.item.connect('selected', () => {
            Util.ensureActorVisibleInScrollView(this._scrollView, usbDevice.item.actor);
            this._selectUsbDevice(usbDevice);
        });
        usbDevice.item.actor.connect('destroy', () => {
            let keyFocus = global.stage.key_focus;
            if (keyFocus && keyFocus.contains(usbDevice.item.actor))
                this._itemBox.grab_key_focus();
        });
    },

    _usbDeviceAdded(usbDevice) {
        let pos = -1;

        this._createUsbDeviceItem(usbDevice);
        this._itemBox.insert_child_at_index(usbDevice.item.actor, pos);
    },

    destroy: function() {
        this.parent();
    },

    _remove: function() {
        var clean_device = {
            name: this._selectedUsbDevice.name,
            bus: this._selectedUsbDevice.bus,
            port: this._selectedUsbDevice.port,
            addr: this._selectedUsbDevice.addr,
            speed: this._selectedUsbDevice.speed,
            spec: this._selectedUsbDevice.spec,
            device_version: this._selectedUsbDevice.device_version,
            vendor_id: this._selectedUsbDevice.vendor_id,
            product_id: this._selectedUsbDevice.product_id,
            class: this._selectedUsbDevice.class,
            subclass: this._selectedUsbDevice.subclass,
            protocol: this._selectedUsbDevice.subclass
        };

        var request = {
            domid: "0",
            device: clean_device
        };

        usbmenu.dbus_server.DetachDevice(request);
        this.close();
    },
});
